<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Latihan 1</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css">
</head>
<body>
    <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid">
          <a class="navbar-brand" href="#">Navbar</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="index.html">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="biodata.html">Biodata</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="berita.html">Berita</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="galeri.html">Galeri</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Dropdown
                </a>
                <ul class="dropdown-menu">
                  <li><a class="dropdown-item" href="berita.html">Berita</a></li>
                  <li><a class="dropdown-item" href="galeri.html">Galeri</a></li>
                  <li><hr class="dropdown-divider"></li>
                  <li><a class="dropdown-item" href="diki">Something else here</a></li>
                </ul>
              </li>
              <li class="nav-item">
                <a class="nav-link disabled" aria-disabled="true">Disabled</a>
              </li>
            </ul>
            <form class="d-flex" role="search">
              <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
              <button class="btn btn-outline-success" type="submit">Search</button>
            </form>
          </div>
        </div>
      </nav>
    <div class="row">
        <div class="col-md-3 p-3">
            <div class="card">
                <div class="card-body">
                    <img src="diki.jpg" class="img-fluid" alt="diki">  
                </div>
            </div>

        </div>
        <div class="col-md-9 p-3">
            <div class="card">
                <div class="card-body bg-primary">
                    <table class="table tabel-hover table-primary">
                        <thead>
                            <h1 class="text-center "><i>BIODATA DIRI</i></h1>
                        </thead>
                        <tbody>
                            <tr>
                                <td>NAMA</td>
                                <td>:</td>
                                <td>Diki Wahyu Prakoso</td>
                            </tr>
                            <tr>
                                <td>NPM</td>
                                <td>:</td>
                                <td>021220046</td>
                            </tr>
                            <tr>
                                <td>PRODI</td>
                                <td>:</td>
                                <td>Sistem Informasi</td>
                            </tr>
                            <tr>
                                <td>AGAMA</td>
                                <td>:</td>
                                <td>Islam</td>
                            </tr>
                            <tr>
                                <td>Jenis Kelamin</td>
                                <td>:</td>
                                <td>Pria</td>
                            </tr>
                            <tr>
                                <td>No.hp</td>
                                <td>:</td>
                                <td>085609305247</td>
                            </tr>
                            <tr>
                                <td>ALAMAT</td>
                                <td>:</td>
                                <td>Komp. Garuda Putra 3</td>
                            </tr>
                            <tr>
                                <td>HOBI</td>
                                <td>:</td>
                                <td>
                                    <ul>
                                        <li>Bola</li>
                                        <li>Gamers</li>
                                        <li>Menyanyi</li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td>sekolah</td>
                                <td>:</td>
                                <td>
                                    <ol>
                                    <li>SD Negri 147 Palembang</li>
                                    <li>SMP Negri 57 Palembang</li>
                                    <li>SMK Utama Bakti Palembang</li>
                                </ol>
                                </td>
                            </tr>
                        </tbody>
                    </table>   
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
